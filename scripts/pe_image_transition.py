#!/usr/bin/env python
from __future__ import print_function

import rospy
import roslib
import yaml
import cv2
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import itertools

roslib.load_manifest('ros_image_transition')

class PeImageTransition:
    def __unregister_subscribers(self):
        if self.__subscriber_a is not None:
            self.__subscriber_a.unregister()
        if self.__subscriber_b is not None:
            self.__subscriber_b.unregister()
        if self.__single_subscriber is not None:
            self.__single_subscriber.unregister()

    def __setup_single_sequence(self):
        self.__single_subscriber = rospy.Subscriber(self.__single_topic, Image, self.__single_sub)
        self.__first_single_image = True
        rospy.loginfo(self.__single_subscriber.name)

    def __setup_sequence(self):
        self.__transition_frames = rospy.get_param('~transition_frames', 25)  # Number of frames to transition
        if self.__transition_frames <= 1:
            self.__transition_frames = 10
            rospy.logwarn("transition_frames > 0. defaulting to 10 frames")

        self.__view_frames = rospy.get_param('~view_frames',
                                             30)  # Number of frame to wait before changing to the next topic
        self.__rviz_viewport_frames = rospy.get_param('~view_frames',
                                                      200)  # Number of frame to wait before changing to the next topic for the rviz viewport
        self.__topic_subscribers = []

        for topic in self.__input_topics:
            self.__topic_subscribers.append(topic)

        if len(self.__topic_subscribers) <= 1 or len(self.__topic_subscribers) > 6:
            rospy.logerr(" 2<= Num Subscribers <= 6. Finishing...");
            exit(1)

        initial_topic = self.__topic_subscribers[-1]  # start with the last one

        self.__topic_iterator = itertools.cycle(self.__topic_subscribers)
        self.__reset_alpha_iterator()
        self.__subscriber_a = rospy.Subscriber(initial_topic, Image, self.__image_a_sub)
        rospy.loginfo(self.__subscriber_a.name)
        self.__subscriber_b = rospy.Subscriber(next(self.__topic_iterator), Image, self.__image_b_sub)
        rospy.loginfo(self.__subscriber_b.name)

        self.__using_a = False
        self.__using_b = False
        self.__image_a = None
        self.__image_b = None
        self.__image_a_header = None
        self.__image_b_header = None

        self.__frame_count = 0  # Static Frames
        self.__wait = False

    def __init__(self):
        rospy.init_node("pe_image_transition", anonymous=True)

        self.__subscriber_a = None
        self.__subscriber_b = None
        self.__single_subscriber = None
        self.__single_topic = 'rviz/view_image'

        self.__output_width = rospy.get_param('~output_width', 1920)
        self.__output_height = rospy.get_param('~output_height', 1080)

        self.__input_topics = rospy.get_param('~input_topics', [
            '/image_rect_color',
            '/overlay_depth',
            '/overlay_intensity',
            '/image_video',
            '/rviz/view_image'
        ])
        self.__setup_sequence()

        self.__output_topic = "image_final"
        self.image_pub = rospy.Publisher(self.__output_topic, Image, queue_size=1)
        rospy.loginfo("Publishing final image to: {} [sensor_msgs/Image]".format(self.__output_topic))

        self.__bridge = CvBridge()
        rospy.spin()

    def __reset_alpha_iterator(self):
        self.__alpha_iterator = iter(np.linspace(0, 1, self.__transition_frames))

    def __resize_if_needed(self):
        #(rows_b, cols_b, ch_b) = self.__image_b.shape
        #if rows_a != self.__output_height or cols_a != self.__output_width:
        self.__image_a = cv2.resize(self.__image_a, (self.__output_width, self.__output_height),
                                    interpolation=cv2.INTER_AREA)
        #if rows_b != self.__output_height or cols_b != self.__output_width:
        self.__image_b = cv2.resize(self.__image_b, (self.__output_width, self.__output_height),
                                    interpolation=cv2.INTER_AREA)

    def __generate(self):
        try:
            if self.__using_a and self.__using_b:
                final_image = None
                if self.__image_a is not None and self.__image_b is not None:
                    if not self.__wait:
                        rospy.loginfo("alpha")
                        try:
                            alpha = next(self.__alpha_iterator)
                            beta = 1 - alpha
                            self.__resize_if_needed()
                            final_image = cv2.addWeighted(self.__image_b, alpha, self.__image_a, beta, 0)

                        except StopIteration:
                            rospy.loginfo("Finished transition")
                            self.__reset_alpha_iterator()
                            self.__wait = True
                    else:  # Wait static
                        self.__resize_if_needed()
                        final_image = self.__image_b
                        self.__frame_count = self.__frame_count + 1
                        rospy.loginfo("Static Wait {}".format(self.__frame_count))
                        total_frames = self.__view_frames
                        if self.__subscriber_b.name == "/rviz/view_image":
                            total_frames = self.__rviz_viewport_frames
                        if self.__frame_count >= total_frames:
                            rospy.loginfo("Switching subscribers")
                            self.__frame_count = 0
                            subscriber_b = self.__subscriber_b.name
                            self.__subscriber_a.unregister()
                            self.__subscriber_b.unregister()
                            self.__subscriber_a = rospy.Subscriber(subscriber_b, Image, self.__image_a_sub)
                            rospy.loginfo(self.__subscriber_a.name)
                            self.__subscriber_b = rospy.Subscriber(next(self.__topic_iterator), Image,
                                                                   self.__image_b_sub)
                            rospy.loginfo(self.__subscriber_b.name)
                            self.__wait = False

                if final_image is not None:
                    ros_image = self.__bridge.cv2_to_imgmsg(final_image, "bgr8")
                    ros_image.header = self.__image_a_header
                    self.image_pub.publish(ros_image)
                    rospy.loginfo("Publishing")

                else:
                    rospy.logerr("Image not generated")
                self.__using_a = False
                self.__using_b = False
        except Exception as e:
            rospy.logwarn(e)

    def __single_sub(self, image_msg):
        try:
            self.image_pub.publish(image_msg)
        except Exception as e:
            rospy.logerr(e)

    def __image_a_sub(self, image_msg):
        if self.__using_a:
            return
        self.__using_a = True
        try:
            self.__image_a = self.__bridge.imgmsg_to_cv2(image_msg, "bgr8")
            self.__image_a_header = image_msg.header
            self.__generate()
        except CvBridgeError as e:
            rospy.logerr(e)

    def __image_b_sub(self, image_msg):
        if self.__using_b:
            return
        self.__using_b = True
        try:
            self.__image_b = self.__bridge.imgmsg_to_cv2(image_msg, "bgr8")
            self.__image_b_header = image_msg.header
            self.__generate()
        except CvBridgeError as e:
            rospy.logerr(e)


if __name__ == "__main__":
    campub = PeImageTransition()
